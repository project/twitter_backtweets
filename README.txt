
Twitter Backtweets provides Backtweets.com integration for your Drupal site. 
Once you have signed up for an API key from http://backtweets.com/developers and 
inserted it into the administrative form, you will begin to pull in any tweets 
posted on Twitter that link to your content. The tweets will be posted as 
comments on the node that was linked to. Even links that have been shortened 
via a URL shortener (e.g. bit.ly, is.gd) will be added.

When cron runs on your site, any nodes posted within the last 48 hours are 
checked and comments are automatically posted if they are new and have not 
already been posted. The Backtweets API allows up to 1000 calls daily, so 
the timeframe limits the risk of reaching the limit. More advanced control 
over time settings are forthcoming.

Twitter Backtweets was written and is maintained by Theresa Summa 
(theresaanna), http://theresaanna.com.